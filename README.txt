Plugin Builder Results

Your plugin FeatureFilterDatasetBuilder was created in:
    /home/parting/repositories/DL-mutation-filter/feature_filter_dataset_builder

Your QGIS plugin directory is located at:
    /home/parting/.local/share/QGIS/QGIS3/profiles/default/python/plugins

What's Next:

  * Copy the entire directory containing your new plugin to the QGIS plugin
    directory

  * Compile the resources file using pyrcc5

  * Run the tests (``make test``)

  * Test the plugin by enabling it in the QGIS plugin manager

  * Customize it by editing the implementation file: ``feature_filter_dataset_builder.py``

  * Create your own custom icon, replacing the default icon.png

  * Modify your user interface by opening FeatureFilterDatasetBuilder_dialog_base.ui in Qt Designer

  * You can use the Makefile to compile your Ui and resource files when
    you make changes. This requires GNU make (gmake)

For more information, see the PyQGIS Developer Cookbook at:
http://www.qgis.org/pyqgis-cookbook/index.html

(C) 2011-2018 GeoApt LLC - geoapt.com
