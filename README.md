# dataset exporter
QGIS Plugin for making an object detection dataset from an annotated vectorlayer and target rasterlayer.

## overview
This plugin is a companion plugin to the `feature_annotator` plugin and it expects a vector layer in the format used by the `feature_annotator` plugin. 
It exports the data to the specified folder in the following filestructure:
```
dataset_name
├── annotation.json
├── images
│   ├── image_0.png
│   ├── image_1.png
│   ├── image_2.png
│   |     :
│   └── image_n.png
└── masks
    ├── mask_0.png
    ├── mask_1.png
    ├── mask_2.png
    |     :
    └── mask_n.png

````
Where the `annotation.json` contains the paths to each image and mask, along with the classifications & general features for each polygon like geolocation and area.


## Installation

### Manual
Download the repository as a `.zip` file and use the QGIS plugin manager (under Plugins -> Manage and Install Plugins) with the option `Install from ZIP`. <br>
Alternatively: <br>
Clone the repository directly to your qgis plugin folder, usually at a location like `/home/<user>/.local/share/QGIS/QGIS3/profiles/default/python/plugins` and activate it in the QGIS plugin manager

### QGIS Python Plugins Repository
Installation from QGIS directly coming in the future.

## Usage
Launch the plugin and select the vectorlayer and rasterlayer to use. Specify the location of the output directory and click on Build dataset. 
There is no progress bar and QGIS will be unresponsive during the export process so give it a bit of time, to check the progress of the plugin you can check if new files are being written to the destination folder.

## contributing
Feel free to contribute by submitting a pull request.

### Maintainer
Code is developed and maintained by j.gerbscheid@hetwaterschapshuis.nl
